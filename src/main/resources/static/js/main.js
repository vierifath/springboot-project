$(document).ready(function () {
  getAllData();

  $("#e_tgl_registrasi").flatpickr({
    enableTime: true,
    dateFormat: "Y-m-d H:i",
  });

  $("#i_tgl_registrasi").flatpickr({
    enableTime: true,
    dateFormat: "Y-m-d H:i",
  });

  $("#tgl_registrasi").flatpickr({
    enableTime: true,
    dateFormat: "Y-m-d H:i",
  });
});

function getAllData() {
  try {
    let table = $("#consumer-table").DataTable({
      destroy: true,
      ajax: {
        url: "/GetAll",
        type: "GET",
        dataSrc: "",
      },
      columns: [
        {
          data: "id",
          name: "id",
          render: function (data, type, row, meta) {
            // Render the index value based on the row position
            return meta.row + 1 + ".";
          },
        },
        {
          data: "id",
          name: "id",
        },
        {
          data: "nama",
          name: "nama",
        },
        {
          data: "alamat",
          name: "alamat",
        },
        {
          data: "kota",
          name: "kota",
        },
        {
          data: "provinsi",
          name: "provinsi",
        },
        {
          data: "tgl_registrasi",
          name: "tgl_registrasi",
          render: function (data, type, row, meta) {
            const dateOnly = data.substring(0, 10);
            return dateOnly;
          },
        },
        {
          data: "status",
          name: "status",
          render: function (data, type, row, meta) {
            return handlingStatus(data);
          },
        },
        {
          data: "id",
          name: "id",
          render: function (data, type, row, meta) {
            return (
              `<div class="d-flex justify-content-center align-items-center align-content-center gap-2 py-0" style="height: 30px;">
                  <button type="button" class="btn btn-outline-primary text-xs act-button" onClick="detailData(` +
              data +
              `)" data-bs-toggle="modal" data-bs-target="#detailData">
                              Detail
                            </button>` +
              `<button type="button" class="btn btn-outline-warning text-xs act-button" onClick="editData(` +
              data +
              `)" data-bs-toggle="modal" data-bs-target="#editData">
                              Edit
                            </button>` +
              `<button type='button' class='btn btn-outline-danger text-xs act-button' onClick='deleteData(` +
              data +
              `)'>Delete</button>
                      </div>`
            );
          },
        },
      ],
    });
  } catch (error) {
    Swal.fire({
      icon: "error",
      title: "Error!",
      text: error,
    });
  }
}

function filterData() {
  let id = $("#f_id").val();
  if (id === "") {
    id = null;
  }
  let nama = $("#f_nama").val();
  if (nama === "") {
    nama = null;
  }
  let alamat = $("#f_alamat").val();
  if (alamat === "") {
    alamat = null;
  }
  let kota = $("#f_kota").val();
  if (kota === "") {
    kota = null;
  }
  let provinsi = $("#f_provinsi").val();
  if (provinsi === "") {
    provinsi = null;
  }
  let status = $("#f_status ").val();
  if (status === "") {
    status = null;
  }

  let body = {
    id: id,
    nama: nama,
    alamat: alamat,
    kota: kota,
    provinsi: provinsi,
    status: status,
  };

  try {
    let table = $("#consumer-table").DataTable({
      destroy: true,
      ajax: {
        url: "/FilterData",
        dataSrc: "",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: function (d) {
          return JSON.stringify(body);
        },
      },
      columns: [
        {
          data: "id",
          name: "id",
          render: function (data, type, row, meta) {
            return meta.row + 1 + ".";
          },
        },
        {
          data: "id",
          name: "id",
        },
        {
          data: "nama",
          name: "nama",
        },
        {
          data: "alamat",
          name: "alamat",
        },
        {
          data: "kota",
          name: "kota",
        },
        {
          data: "provinsi",
          name: "provinsi",
        },
        {
          data: "tgl_registrasi",
          name: "tgl_registrasi",
          render: function (data, type, row, meta) {
            const dateOnly = data.substring(0, 10);
            return dateOnly;
          },
        },
        {
          data: "status",
          name: "status",
          render: function (data, type, row, meta) {
            return handlingStatus(data);
          },
        },
        {
          data: "id",
          name: "id",
          render: function (data, type, row, meta) {
            return (
              `<div class="d-flex justify-content-center align-items-center align-content-center gap-2 py-0" style="height: 30px;">
                                <button type="button" class="btn btn-outline-primary text-xs act-button" onClick="detailData(` +
              data +
              `)" data-bs-toggle="modal" data-bs-target="#detailData">
                                            Detail
                                          </button>` +
              `<button type="button" class="btn btn-outline-warning text-xs act-button" onClick="editData(` +
              data +
              `)" data-bs-toggle="modal" data-bs-target="#editData">
                                            Edit
                                          </button>` +
              `<button type='button' class='btn btn-outline-danger text-xs act-button' onClick='deleteData(` +
              data +
              `)'>Delete</button>
                                    </div>`
            );
          },
        },
      ],
    });
    Swal.fire({
      icon: "success",
      title: "Success!",
      text: "Success in filtering the data!",
    });
  } catch (error) {
    Swal.fire({
      icon: "error",
      title: "Error!",
      text: error,
    });
  }
}

function handlingStatus(param) {
  switch (param) {
    case "0":
      param = `<span class="badge bg-secondary" style= "width: 100px; display: inline-block;">Non-Aktif</span>`;
      break;
    case "1":
      param = `<span class="badge bg-success" style= "width: 100px; display: inline-block;">Aktif</span>`;
      break;
  }

  return param;
}

function handlingStatus2(param) {
  switch (param) {
    case "0":
      data = "Non-Aktif";
      break;
    case "1":
      data = "Aktif";
      break;
  }

  return data;
}

function handlingDateTime(param) {
  const momentObj = moment(param);
  const formattedDateTime = momentObj.format("YYYY-MM-DD HH:mm");

  return formattedDateTime;
}

function handlingDateTime2(param) {
  let parts = param.split(" ");
  let datePart = parts[0];
  let timePart = parts[1];

  let formattedDatetime = datePart + "T" + timePart + ":00";
  return formattedDatetime;
}

function detailData(id) {
  let body = {
    id: id,
  };

  $.ajax({
    url: "/GetDetail",
    type: "POST",
    contentType: "application/json",
    dataType: "text",
    data: JSON.stringify(body),
    success: function (data) {
      const obj = JSON.parse(data);

      $("#id").val(obj.id);
      $("#nama").val(obj.nama);
      $("#alamat").val(obj.alamat);
      $("#kota").val(obj.kota);
      $("#provinsi").val(obj.provinsi);
      $("#tgl_registrasi").val(handlingDateTime(obj.tgl_registrasi));
      $("#status").val(handlingStatus2(obj.status));

      $("#createdform")[0].reset();
    },
    error: function (xhr, status, e) {
      Swal.fire({
        icon: "error",
        title: "Error!",
        text: "Failed to show data!",
      });
    },
  });
}

function storeData() {
  const nama = $("#i_nama").val();
  const alamat = $("#i_alamat").val();
  const kota = $("#i_kota").val();
  const provinsi = $("#i_provinsi").val();
  const tglRegistrasi = handlingDateTime2($("#i_tgl_registrasi").val());
  const status = $("#i_status").val();

  let body = {
    nama: nama,
    alamat: alamat,
    kota: kota,
    provinsi: provinsi,
    tgl_registrasi: tglRegistrasi,
    status: status,
  };

  $.ajax({
    url: "/StoreData",
    type: "POST",
    contentType: "application/json",
    dataType: "text",
    data: JSON.stringify(body),
    success: function (data) {
      const resmsg = JSON.parse(data);

      if (resmsg.resCode === 200) {
        $("#inputData").modal("hide");
        getAllData();
        Swal.fire({
          icon: "success",
          title: "Success!",
          text: "Success to create data!",
        });
        $("#createdform")[0].reset();
      } else {
        $("#inputData").modal("hide");
        getAllData;
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: "Failed to create data!",
        });
      }
    },
    error: function (xhr, status, e) {
      $("#inputData").modal("hide");
      getAllData;
      Swal.fire({
        icon: "error",
        title: "Error!",
        text: "Failed to create data!",
      });
    },
  });
}

function editData(id) {
  let body = {
    id: id,
  };

  $.ajax({
    url: "/GetDetail",
    type: "POST",
    contentType: "application/json",
    dataType: "text",
    data: JSON.stringify(body),
    success: function (data) {
      const obj = JSON.parse(data);


      $("#e_id").val(obj.id);
      $("#e_nama").val(obj.nama);
      $("#e_alamat").val(obj.alamat);
      $("#e_kota").val(obj.kota);
      $("#e_provinsi").val(obj.provinsi);
      $("#e_tgl_registrasi").val(handlingDateTime(obj.tgl_registrasi));
      $("#e_status").val(obj.status);
    },
    error: function (xhr, status, e) {
      Swal.fire({
        icon: "error",
        title: "Error!",
        text: "Failed to show data!",
      });
    },
  });
}

function updateData() {
  const id = $("#e_id").val();
  const nama = $("#e_nama").val();
  const alamat = $("#e_alamat").val();
  const kota = $("#e_kota").val();
  const provinsi = $("#e_provinsi").val();
  const tglRegistrasi = handlingDateTime2($("#e_tgl_registrasi").val());
  const status = $("#e_status").val();

  let body = {
    id: id,
    nama: nama,
    alamat: alamat,
    kota: kota,
    provinsi: provinsi,
    tgl_registrasi: tglRegistrasi,
    status: status,
  };

  $.ajax({
    url: "/UpdateData",
    type: "POST",
    contentType: "application/json",
    dataType: "text",
    data: JSON.stringify(body),
    success: function (data) {
      const resmsg = JSON.parse(data);

      if (resmsg.resCode === 200) {
        $("#editData").modal("hide");
        getAllData();
        Swal.fire({
          icon: "success",
          title: "Success!",
          text: "Success to edit data!",
        });
        $("#updatedForm")[0].reset();
      } else {
        $("#editData").modal("hide");
        getAllData();
        Swal.fire({
          icon: "error",
          title: "Error!",
          text: "Failed to edit data!",
        });
      }
      $("#e_status").val(handlingStatus2(obj.status));
    },
    error: function (xhr, status, e) {
      $("#editData").modal("hide");
      getAllData();
      Swal.fire({
        icon: "error",
        title: "Error!",
        text: "Failed to edit data!",
      });
    },
  });
}

function deleteData(id) {
  let body = {
    id: id,
  };

  Swal.fire({
    title: "Are you sure?",
    text: "Are you sure to remove the data!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1AB394",
    cancelButtonColor: "#C1C1C1",
    confirmButtonText: "Yes!",
    reverseButtons: true,
  }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        url: "/DeleteData",
        type: "POST",
        contentType: "application/json",
        dataType: "text",
        data: JSON.stringify(body),
        success: function (data) {
          Swal.fire({
            icon: "success",
            title: "Success!",
            text: "Success to remove data!",
          });
          getAllData();
        },
        error: function (xhr, status, e) {
          Swal.fire({
            icon: "error",
            title: "Error!",
            text: "Failed to remove data!",
          });
        },
      });
    }
  });
}

function clearFilter() {
  $("#filterForm")[0].reset();
  getAllData();
}
