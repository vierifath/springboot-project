package com.demoproject.model;

import java.time.LocalDateTime;
import java.util.Date;

import jakarta.annotation.Nullable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table (name="consumer")
public class Consumer {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  @Nullable
  private String nama;

  @Column
  @Nullable
  private String alamat;

  @Column
  @Nullable
  private String kota;

  @Column
  @Nullable
  private String provinsi;

  @Column
  @Nullable
  private LocalDateTime tgl_registrasi;

  @Column
  @Nullable
  private String status;

  public Consumer() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNama() {
    return nama;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }

  public String getAlamat() {
    return alamat;
  }

  public void setAlamat(String alamat) {
    this.alamat = alamat;
  }

  public String getKota() {
    return kota;
  }

  public void setKota(String kota) {
    this.kota = kota;
  }

  public String getProvinsi() {
    return provinsi;
  }

  public void setProvinsi(String provinsi) {
    this.provinsi = provinsi;
  }

  public LocalDateTime getTgl_registrasi() {
    return tgl_registrasi;
  }

  public void setTgl_registrasi(LocalDateTime tgl_registrasi) {
    this.tgl_registrasi = tgl_registrasi;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
