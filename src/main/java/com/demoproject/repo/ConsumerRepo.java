package com.demoproject.repo;

import com.demoproject.model.Consumer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface ConsumerRepo extends JpaRepository<Consumer, Long> {

  @Query("SELECT DISTINCT c.kota FROM Consumer c WHERE c.provinsi = :provinsi")
  List<String> findCities(@Param("provinsi") String provinsi);

  @Query("SELECT c FROM Consumer c WHERE (:id IS NULL OR c.id = :id) " +
          "AND (:nama IS NULL OR c.nama LIKE %:nama%) " +
          "AND (:alamat IS NULL OR c.alamat = :alamat) " +
          "AND (:kota IS NULL OR c.kota = :kota) " +
          "AND (:provinsi IS NULL OR c.provinsi = :provinsi) " +
          "AND (:status IS NULL OR c.status = :status)")
  List<Consumer> getFilteredData(@Param("id") Long id, @Param("nama") String nama, @Param("alamat") String alamat,
                                 @Param("kota") String kota, @Param("provinsi") String provinsi,
                                 @Param("status") String status);
}


