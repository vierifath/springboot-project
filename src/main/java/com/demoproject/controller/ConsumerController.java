package com.demoproject.controller;

import com.demoproject.model.Consumer;
import com.demoproject.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ConsumerController {

  @Autowired
  ConsumerService consumerService;

  @RequestMapping(value = "/GetAll", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Consumer> getAll() throws Exception {
    return consumerService.getAll();
  }
  @RequestMapping(value = "/GetDetail", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public Consumer getDetail(@RequestBody Consumer requestBody) throws Exception {
    return consumerService.getDetail(requestBody);
  }

  @RequestMapping(value = "/StoreData", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public String storeData(@RequestBody Consumer requestBody) throws Exception {
    return consumerService.storeData(requestBody);
  }

  @RequestMapping(value = "/UpdateData", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public String updateData(@RequestBody Consumer requestBody) throws Exception {
    return consumerService.updateData(requestBody);
  }

  @RequestMapping(value = "/DeleteData", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public String deleteData(@RequestBody Consumer requestBody) throws Exception {
    return consumerService.deleteData(requestBody);
  }

  @RequestMapping(value = "/FindCities", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public List<String> findCities(@RequestBody Consumer requestBody) throws Exception {
    return consumerService.findCities(requestBody);
  }

  @RequestMapping(value = "/FilterData", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Consumer> filterData(@RequestBody Consumer requestBody) throws Exception {
    return consumerService.filterData(requestBody);
  }



}
