package com.demoproject.service;

import com.demoproject.model.Consumer;
import com.demoproject.repo.ConsumerRepo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.persistence.EntityNotFoundException;
import org.hibernate.service.spi.ServiceException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ConsumerService {

  @Autowired
  ConsumerRepo consumerRepo;

  public List<Consumer> getAll() {
    try {
      ArrayList<Consumer> consumerList = new ArrayList<>();
      consumerRepo.findAll().forEach(consumer -> consumerList.add(consumer));
      return consumerList;

    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }

  public Consumer getDetail(Consumer request) {
    try {
      Consumer data = consumerRepo.findById(request.getId()).get();
      return data;

    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }

  public String storeData(Consumer request) {

    JSONObject resmsg = new JSONObject();
    try {
        consumerRepo.save(request);
        resmsg.put("resCode", 200);
        resmsg.put("message", "Data stored successfully!");

        return resmsg.toString();

    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }

  public String updateData(Consumer request) {

    JSONObject resmsg = new JSONObject();
    try {
      Consumer data = consumerRepo.findById(request.getId()).orElse(null);
      if (data == null) {
        resmsg.put("resCode", 404);
        resmsg.put("message", "Data not found!");

      } else {

        data.setNama(request.getNama());
        data.setAlamat(request.getAlamat());
        data.setKota(request.getKota());
        data.setProvinsi(request.getProvinsi());
        data.setTgl_registrasi(request.getTgl_registrasi());
        data.setStatus(request.getStatus());

        consumerRepo.save(data);

        resmsg.put("resCode", 200);
        resmsg.put("message", "Data edited successfully!");
      }
      return resmsg.toString();

    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }

  public String deleteData(Consumer request) {

    JSONObject resmsg = new JSONObject();
    try {
      Consumer data = consumerRepo.findById(request.getId()).orElse(null);
      if (data == null) {
        resmsg.put("resCode", 404);
        resmsg.put("message", "Data not found!");
      } else {
        consumerRepo.deleteById(data.getId());
        resmsg.put("resCode", 200);
        resmsg.put("message", "Data deleted successfully!");
      }
      return resmsg.toString();

    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }

  public List<String> findCities(Consumer request) {
    try {
      return consumerRepo.findCities(request.getProvinsi());
    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }

  public List<Consumer> filterData(Consumer request) {
    try {
      Long id = request.getId();
      String name = request.getNama();
      String alamat= request.getAlamat();
      String city = request.getKota();
      String province = request.getProvinsi();
      String status = request.getStatus();

      return consumerRepo.getFilteredData(id, name, alamat, city, province, status);
    } catch (Exception ex) {
      throw new ServiceException("An unexpected error occurred.", ex);
    }
  }


}
