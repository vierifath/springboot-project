<!-- Modal -->
<div
  class="modal fade"
  id="editData"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="getId">Edit Data</h5>
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        <form id="updatedForm" method="post">
          <div class="row">
            <div class="col-md-6">
              <!-- First column -->
              <div class="mb-3">
                <label for="input1" class="form-label">ID</label>
                <input type="number" class="form-control" id="e_id" name="e_id" readonly/>
              </div>
              <div class="mb-3">
                <label for="input2" class="form-label">Nama</label>
                <input type="text" class="form-control" id="e_nama" name="e_nama" />
              </div>
              <div class="mb-3">
                <label for="input3" class="form-label">Alamat</label>
                <input type="text" class="form-control" id="e_alamat" name="e_alamat" />
              </div>
              <div class="mb-3">
                <label for="input4" class="form-label">Kota</label>
                <input type="text" class="form-control" id="e_kota" name="e_kota" />
              </div>
            </div>
            <div class="col-md-6">
              <!-- Second column -->
              <div class="mb-3">
                <label for="input5" class="form-label">Provinsi</label>
                <input type="text" class="form-control" id="e_provinsi" name="e_provinsi" />
              </div>
              <div class="mb-3">
                <label for="input6" class="form-label">Tanggal Registrasi</label>
                <input type="text" class="form-control" id="e_tgl_registrasi" name="e_tgl_registrasi"/>
              </div>
              <div class="mb-3">
                <label for="input7" class="form-label">Status</label>
                <select class="form-select" id="e_status" name="e_status">
                  <option hidden></option>
                  <option value="1">Aktif</option>
                  <option value="0">Non-Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
          Close
        </button>
        <button type="button" onClick="updateData()" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
