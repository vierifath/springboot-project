<!-- Modal -->
<div
  class="modal fade"
  id="detailData"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dtTitle">Detail Data</h5>
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        <form id="detailForm">
          <div class="row">
            <div class="col-md-6">
              <!-- First column -->
              <div class="mb-3">
                <label for="input1" class="form-label">ID</label>
                <input type="number" class="form-control" id="id" readonly/>
              </div>
              <div class="mb-3">
                <label for="input2" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" readonly/>
              </div>
              <div class="mb-3">
                <label for="input3" class="form-label">Alamat</label>
                <input type="text" class="form-control" id="alamat" readonly/>
              </div>
              <div class="mb-3">
                <label for="input4" class="form-label">Kota</label>
                <input type="text" class="form-control" id="kota" readonly/>
              </div>
            </div>
            <div class="col-md-6">
              <!-- Second column -->
              <div class="mb-3">
                <label for="input5" class="form-label">Provinsi</label>
                <input type="text" class="form-control" id="provinsi" readonly />
              </div>
              <div class="mb-3">
                <label for="input6" class="form-label">Tanggal Registrasi</label>
                <input type="text" class="form-control" id="tgl_registrasi" readonly/>
              </div>
              <div class="mb-3">
                <label for="input7" class="form-label">Status</label>
                <input type="text" class="form-control" id="status" readonly/>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>
