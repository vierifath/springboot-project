<!-- Modal -->
<div
  class="modal fade"
  id="inputData"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="getId">Input Data</h5>
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        <form id="createdform">
          <div class="row">
            <div class="col-md-6">
              <!-- First column -->
              <div class="mb-3">
                <label for="input2" class="form-label">Nama</label>
                <input
                  type="text"
                  class="form-control"
                  id="i_nama"
                  name="i_nama"
                />
              </div>
              <div class="mb-3">
                <label for="input3" class="form-label">Alamat</label>
                <input
                  type="text"
                  class="form-control"
                  id="i_alamat"
                  name="i_alamat"
                />
              </div>
              <div class="mb-3">
                <label for="input4" class="form-label">Kota</label>
                <input
                  type="text"
                  class="form-control"
                  id="i_kota"
                  name="i_kota"
                />
              </div>
            </div>
            <div class="col-md-6">
              <!-- Second column -->
              <div class="mb-3">
                <label for="input5" class="form-label">Provinsi</label>
                <input
                  type="text"
                  class="form-control"
                  id="i_provinsi"
                  name="i_provinsi"
                />
              </div>
              <div class="mb-3">
                <label for="input6" class="form-label"
                  >Tanggal Registrasi</label
                >
                <input
                  type="text"
                  class="form-control"
                  id="i_tgl_registrasi"
                  name="i_tgl_registrasi"
                />
              </div>
              <div class="mb-3">
                <label for="input7" class="form-label">Status</label>
                <select class="form-select" id="i_status" name="i_status">
                  <option hidden></option>
                  <option value="1">Aktif</option>
                  <option value="0">Non-Aktif</option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
          Close
        </button>
        <button type="button" onClick="storeData()" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
