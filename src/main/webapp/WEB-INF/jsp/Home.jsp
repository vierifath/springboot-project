<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Demo Project</title>

    <!-- library -->

    <!-- bootstrap5 -->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/css/bootstrap.min.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/js/bootstrap.bundle.min.js"></script>

    <!-- argon dashboard -->
    <link
      rel="apple-touch-icon"
      sizes="76x76"
      href="/assets/img/apple-icon.png"
    />
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>Argon Dashboard 2 by Creative Tim</title>
    <!--     Fonts and icons     -->
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      rel="stylesheet"
    />
    <!-- Nucleo Icons -->
    <link href="/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script
      src="https://kit.fontawesome.com/42d5adcbca.js"
      crossorigin="anonymous"
    ></script>
    <link href="/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link
      id="pagestyle"
      href="/assets/css/argon-dashboard.css?v=2.0.4"
      rel="stylesheet"
    />

    <!-- Datatable -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

    <link
      rel="stylesheet"
      href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css"
    />
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>

    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Select2 -->
    <link
      href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css"
      rel="stylesheet"
    />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- flatpckr -->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <!-- moment.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
</head>

    <!-- local file -->
    <link href="/css/styles.css" rel="stylesheet" />
    <script src="/js/main.js" type="text/javascript"></script>
    <jsp:include page="DetailModal.jsp" />
    <jsp:include page="EditModal.jsp" />
    <jsp:include page="InputModal.jsp" />
  </head>

  <body class="g-sidenav-show bg-gray-100">
    <div class="min-height-300 bg-primary position-absolute w-100"></div>
    <aside
      class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4"
      id="sidenav-main"
    >
      <div class="sidenav-header">
        <i
          class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
          aria-hidden="true"
          id="iconSidenav"
        ></i>
        <a
          class="navbar-brand m-0"
          href=" https://demos.creative-tim.com/argon-dashboard/pages/dashboard.html "
          target="_blank"
        >
          <img
            src="/assets/img/logo-ct-dark.png"
            class="navbar-brand-img h-100"
            alt="main_logo"
          />
          <span class="ms-1 font-weight-bold">Demo Project</span>
        </a>
      </div>
      <hr class="horizontal dark mt-0" />
      <div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="">
              <div
                class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center"
              >
                <i class="ni ni-tv-2 text-primary text-sm opacity-10"></i>
              </div>
              <span class="nav-link-text ms-1">Dashboard</span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <main class="main-content position-relative border-radius-lg">
      <!-- Navbar -->
      <nav
        class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl"
        id="navbarBlur"
        data-scroll="false"
      >
        <div class="container-fluid py-1 px-3">
          <nav aria-label="breadcrumb">
            <ol
              class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5"
            >
              <li class="breadcrumb-item text-sm">
                <a class="opacity-5 text-white" href="javascript:;">Pages</a>
              </li>
              <li
                class="breadcrumb-item text-sm text-white active"
                aria-current="page"
              >
                Tables
              </li>
            </ol>
            <h6 class="font-weight-bolder text-white mb-0">Consumer</h6>
          </nav>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="container-fluid py-4">
        <div class="row">
          <div class="col-12">
            <div class="card mb-4 px-4">
              <div class="card-header d-flex justify-content-start pt-5 pb-0">
                <h4>Consumer Data</h4>
              </div>
              <div class="card-body pb-5">
                <div class="d-flex pb-2">
                  <form id="filterForm" method="post" style="width: 100%" class="text-xs">
                    <div class="row">
                      <div class="col">
                        <div class="mb-3">
                          <label for="input1" class="form-label">ID</label>
                          <input type="number" class="form-control" id="f_id" name="f_id" />
                        </div>
                        <div class="mb-3">
                          <label for="input2" class="form-label">Nama</label>
                          <input type="text" class="form-control" id="f_nama" name="f_nama" />
                        </div>
                        <div class="mb-3">
                          <label for="input3" class="form-label">Alamat</label>
                          <input type="text" class="form-control" id="f_alamat" name="f_alamat" />
                        </div>
                      </div>
                      <div class="col">
                        <div class="mb-3">
                          <label for="input4" class="form-label">Kota</label>
                          <input type="text" class="form-control" id="f_kota" name="f_kota" />
                        </div>
                        <div class="mb-3">
                          <label for="input5" class="form-label">Provinsi</label>
                          <input type="text" class="form-control" id="f_provinsi" name="f_provinsi" />
                        </div>
                        <div class="mb-3">
                          <label for="input6" class="form-label">Status</label>
                          <select class="form-select" id="f_status" name="f_status">
                            <option hidden></option>
                            <option value="1">Aktif</option>
                            <option value="0">Non-Aktif</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="d-flex justify-content-end align-items-end align-content-end pb-5 gap-3">
                  <button type="button" class="btn bg-gradient-light text-xs" onClick="clearFilter()">
                    Clear Filter
                  </button>
                  <button type="button" onClick="filterData()" class="btn bg-gradient-primary text-xs">Retrieve</button>
                </div>

                <div
                  class="d-flex justify-content-start align-items-end align-content-end pb-2"
                >
                  <button type="button" data-bs-toggle="modal" data-bs-target="#inputData" class="btn bg-gradient-primary text-xs">Input Data</button>
                </div>

                <div class="table-responsive text-xs">
                  <table id="consumer-table" class="table text-xs align-items-center" style="width: 100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>Tanggal Registrasi</th>
                        <th>Status</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!--   Core JS Files   -->
    <script src="/assets/js/core/popper.min.js"></script>
    <script src="/assets/js/core/bootstrap.min.js"></script>
    <script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script>
      var win = navigator.platform.indexOf("Win") > -1;
      if (win && document.querySelector("#sidenav-scrollbar")) {
        var options = {
          damping: "0.5",
        };
        Scrollbar.init(document.querySelector("#sidenav-scrollbar"), options);
      }
    </script>
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="/assets/js/argon-dashboard.min.js?v=2.0.4"></script>

    <!-- Modal -->
  </body>
</html>
